<?php 

function dbConnect() {
    /* 1. Connecter au SGBD Mysql */
    $dbh = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
    /* On dit à PDO de lever des exceptions en cas d'erreurs (dans les requêtes par exemples) */
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $dbh;
}