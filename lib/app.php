<?php

/** Ajoute un message dans le FlashBag
 * @param string $level niveau du message (warning, danger, info, success )
 * @param string $message le message à transmettre
 */
function flashbagAdd(string $level, string $message) {

    if(!isset($_SESSION['flashbag']))
        $_SESSION['flashbag'] = [];

    $_SESSION['flashbag'][] = ['level'=>$level, 'message'=>$message];
}

/** Retourne les messages présents dans le FlashBag et vide la session FlashBag
 * @return array les messages
 */
function flashbagReadAll() {
    $flashbag = null;

    /* Si on a des messages on les récupère et on vide la session $_SESSION['flashbag'] */
    if (isset($_SESSION['flashbag'])) {
        $flashbag = $_SESSION['flashbag'];
        unset($_SESSION['flashbag']);
    }
    
    return $flashbag;
}

/** Vérifie les uploads et retourne le nom du fichier à mettre en base
 * @param string nom 
 * @param string chemin d'accès 
 * @param array tableau erreurs par référence 
 */
function appUpload() {

}