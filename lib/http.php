<?php

/** Premère version de la fonction sans paramètres spécifiques */
function httpGetUrl(string $controller=null, array $params=[]) : string {

    $queryString = '';
    foreach($params as $param=>$value)
        $queryString .= (($queryString!='')?'&':'').$param.'='.$value;

    if($controller!== null)
        return 'index.php?controller='.$controller.(($queryString!='')?'&':'').$queryString;

    return 'index.php'.(($queryString!='')?'?':'').$queryString;
}