<?php

/** Vérifie si un utilisateur a le droit d'accèder à une page 
 * 
 */
function securUserIsConnected(string $role=null)
{
    if(session_status() != PHP_SESSION_ACTIVE )
        session_start();

    if (!isset($_SESSION['connected']) || $_SESSION['connected'] !== true) {
        // redirection vers le login
        header('Location:'.httpGetUrl('login'));
        exit();
    }
    
    if($role !== null && $_SESSION['connected'] === true && ($_SESSION['user']['role'] != 'ROLE_ADMIN' && $role != $_SESSION['user']['role']))
    {
        // l'utilisateur n'a pas le bon rôle pour être ici ;) On redirige vers l'accueil
        header('Location:'.httpGetUrl('home'));
        exit();
    }
}