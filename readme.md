# Le blog

Nous allons créer un outil complet pour la gestion d'un blog et la création d'un blog.
Notre application devra se composer d'un BackOffice pour administrer notre Blog et d'un Front pour les internautes qui veulent parcourir notre blog, lire les articles, laisser un commentaire.

Le backOffice sera sécurisé et seul les utilisateurs présenta dans la table `user` pourront se connecter.

Sur le Front les personnes pourront laisser un commentaire sans s'identifier. Ils devront fournir leur email et leur pseudo uniquement.

Il ne sera pas possible pour un utilisateur quelconque de s'inscrire dans la table user. Seul l'administrateur pourra rajouter un auteur dans la table via le BackOffice.

En gros le fonctionnement de notre projet sera le même qu'un mini Wordpress ;)


## Etape 1 : modélisation de la base de données

Nous allons modéliser la base de données sous MysqlWorkBench avec les informations suivantes sur les tables.
Pour la modélisation il faut installer MysqlWorkBench. 
Puis dans le menu `File > New Model` ou `Fichier > Nouveau Model`
Ensuite vous devez renomer votre base de données de MyDb vers Blog puis créer un diagramme :

![Image](./imgTuto/MySQLWorkbench1.png)

Dans le diagramme vous devez ajouter une première table, puis la renommer (par exemple **article**) :

![Image](./imgTuto/MySQLWorkbench2.png)

![Image](./imgTuto/MySQLWorkbench3.png)

Il faut ensuite ajouter nos noms de colonnes et leur propriétés :

![Image](./imgTuto/MySQLWorkbench4.png)

>
> Ajouter tous les champs (colonnes) à la table **article** puis faîtes toutes vos tables. 
>
> Reportez vous pour ça aux définitions de nos table ci-après.
>
> Nous passerons ensuite aux relations et à la cardinalité des ces relations entre nos table !
>

** A savoir : **

> **PK** : primaryKey
>
> **NN** : not null
>
> **UQ** : unique key (pour l'email de l'utilisateur par exemple, pas de doublon dans la base)
>
> **B**  : binarie : données binaire (Inutile pour nous !)
>
> **UN** : unsigned : des chiffres positifs seulement 
>
> **ZF** : zero forced : si numéric  rajoute des 0 devant le nombre pour stocker un nombre de caractère donné. Exemple si INT(11) et 50000 dedans, Mysql stockera en réalité 00000050000 dans la table)
>
> **AI** : auto increment : pour les clefs primaires, obligatoire pour nous
>
> **G** : générated column : colonne générée à partir d'autres colonnes (inutile pour nous)

Vous pouvez sélectionner le type de colonne (INT, VARCHAR, TEXT, DATETIME) et donnez des paramètre (INT(11) un entier sur 4 octets avec 11 chiffres maximum, ou VARCHAR(255) une chaine de 255 caractères maximum)

Pour comprendre la différences entre BYTES, bit, Octet :

[Infos BYTES, bit, octet](https://www.science.lu/fr/les-unites-informatiques/quest-ce-que-les-bit-byte-octet)

Pour comprendre les types de champs possible dans MySql et leur taille :

[Tuto SQL TYPE](https://openclassrooms.com/fr/courses/1959476-administrez-vos-bases-de-donnees-avec-mysql/1960456-distinguez-les-differents-types-de-donnees)



### Table article
    - id (int PrimaryKey autoIncrement unsigned)
    - title (varchar not null)
    - content (text)
    - picture (varchar)
    - createdAt (date not null)
    - publishedAt (date not null)
    - valid (boolean not null default=true)

### Table category
    - id (int PrimaryKey autoIncrement unsigned)
    - title (varchar not null)
    - description (text)
    - picture (varchar)
    - createdAt (date not null)

### Table comment
    - id (int PrimaryKey autoIncrement unsigned)
    - username (varchar not null)
    - email (varchar not null)
    - comment (text not null)
    - createdAt (date not null)
    - valid (boolean not null default=true)

### Table user
    - id (int PrimaryKey autoIncrement unsigned)
    - firstname (varchar not null)
    - lastname (varchar not null)
    - email (varchar not null)
    - password (varchar not null)
    - avatar (varchar)
    - createdAt (date not null)
    - bio (text)
    - valid (boolean not null default=true)
    - role (varchar not null default=ROLE_USER)


### Nous devons maintenant ajouter les relations

Pour le moment dans les colonnes proposées ci-dessus nous n'avons pas les clefs étrangères. 

Le fait de faire ces relations va créer ces clefs automatiquement.

Par exemple dans la table `article` nous auront 2 colonnes de plus pour la clef étrangère de la catégorie et celle de l'utilisateur (auteur de l'article). 

**Les relations sont les suivantes :** 

> 1 user peut avoir plusieurs articles (cardinalité 1 à n)
>
> 1 catégorie peut avoir plusieurs articles (cardinalité 1 à n)
>
> 1 article peut avoir plusieurs commentaires (cardinalité 1 à n)

**Pour créer nos relations**

![Image](./imgTuto/MySQLWorkbench5.png)

On sélectionne dans le menu la relation souhaitée (on utilise uniqurement les relations non identifiées représentées avec des pointilés).

Cliquez ensuite sur le table de droite (n) puis sur la table de gauche (1) pour une cardinalité de 1 à n !

**C'est parti notre base est modélisée**

### Creer un fichier SQL à partir de notre MCD

Maintenant que notre MCD est fini il nous faut déjà l'enregistrer.

Puis nous allons exporter ce MCD en requête SQL pour l'executer sur notre serveur et ainsi créer notre base de données.

Dans le menu `File > Export > Forward Engineer....`

![Image](./imgTuto/MySQLWorkbench6.png)

>
> Etape 1 : Laissez le nom du fichier vide et ne cocher aucune case. Puis Next >
>
> Etape 2 : Laissez la case 1 cochée. Ne cocher rien d'autre. Puis Next >
>
> Etape 3 : copier le code SQL proposée dans son intégralité (clique dans le champs puis CTRL+A puis CTRL+C )
>
> Etape 5 : rendez-vous sur phpMyadmin. Le script SQL va créer une base blog. Si vous en avez déjà une vous devez la renommer, ou supprimer la table user à l'intérieur, elle sera recrée par notre script.
>
> Etape 6 : dans l'accueil de phpMyadmin, cliquez sur l'onglet SQL en haut à droite 
>
> Etape 7 : coller le sql dans le champ et faire executer !
>
> La base est maintenant créée !
>


## Etape 2 : conception de l'architecture pour le BackOffice.

Le backOffice d'un site est en fait un site à part entière qui permet de modifier les données du site.
Il est souvent dans un dossier 'admin' à la racine de notre site.
Par exemple Wordpress c'est le dossier `wp_admin`.
Toutefois ici nous allons passer par une seule et unique porte d'entrée à la fois pour notre Front et pour notre Back. Un peu à la façon de Symfony.
Notre fichier `index.php` à la racine sera capable de servir des pages de l'administration ou des pages du Front. 

**Nous allons respecter cette arborescence :**

```
config
    config.php

assets
    js
    img
    css

src
    models
    controllers

views
    admin
    front

lib
    classes

index.php 
```

Le dossier `config`  Il contiendra le(s) fichier(s) de configuration

Le dossier `assets` contiendra tous les assets HTML (le js, le css, les images)

Le dossier `src`    contiendra nos développements (controllers et models)

Le dossier `view`   contiendra les vues html (phtml)

Le dossier `lib`    contiendra les librairies de fonction ou de classes

Le fichier `index.php`  sera notre FrontController mais aussi routeur simplifié

    
## Etape 3 : greffer notre système de login et ajout user

La première chose à faire va être de greffer notre système de login dans cette architecture.
Nous allons donc importer nos fichiers controller, views, models.

Si vous avez codé sans controller ni model vous allez devoir adapter votre code en récupérant ce qui a été fait pour classicModels dans la brache GIT classicModelsMVC sur Gitlab !

Nous allons donc créer un fichier `index.php` à la racine de notre dossier et y mettre le code présent dans le fichier `index.php` de classicModel en mode MVC. Cette page sera la porte d'entrée de notre application de backOffice.

**index.php**
```php
declare(strict_types=1);

/* On inclu la configuration */
require('../config/config.php');

/* On inclu la libraire */
require('lib/bdd.php');

/** La constante DEFAULT_CONTROLLER doit-être défini dans le fichier config.php
 * const DEFAULT_CONTROLLER = 'home';
 * C'est le controller par défaut si on en fourni pas. Par exemple *home* qui affichera le Dashbord du backOffice 
 */
$controller = DEFAULT_CONTROLLER;

/** Si le controller est passé dans l'URI on le récupère **/
if(isset($_GET['controller']))
    $controller = $_GET['controller'];

/** Si le fichier n'existe pas dans le dossier des controller on lève une exeption
 * cont PATH_SRC = 'src/'; //a définir dans config.php
 */

try
{
    if(!file_exists(PATH_SRC.'controllers/'.$controller.'.php'))
        throw new DomainException('Le controller demandé n\'existe pas');
    
    /* On inclu le controller */
    require('controllers/'.$controller.'.php');
}
catch (DomainException $e) {
    /** On répond le contenu du fichier error.phtml (template complet) qui affichera le contenu de la variable $message. On précise aussi le code de réponse 404 (ressource non trouvée !)
    * cont PATH_VIEW = 'views/'; //a définir dans config.php
    */
    $message = $e->get_message();
    require(PATH_VIEW.'error.phtml');
    exit();
}

```
Pour charger la page d'ajout d'un user il faudra donc appeler : `index.php?controller=addUser`
Et surtout pas appeler le controller directement.

Seul le fichier `index.php` doit-être présent dans l'URL.

Je vous conseille de faire une fonction `getURL(string $controller, array $params) : string` dans une librairie `http.php`.

Cette fonction nous retournera l'url de notre page. Par exemple pour afficher le détail d'un utilisateur :

```php
/* récupérer le lien vers la fiche d'un user (controller *user* avec argument id=10)*/
$urlUserDetail = getUrl('user',['id'=>10]);

/* nous obtenons dans $urlUserDetail la valeur : index.php?controller=user&id=10 */
```

Ainsi nous pourrons modifier cette fonction pour que nos lien s'adapte par exemple à un processus d'URLRewriting.


## Etape 4 : c'est parti !

On développe toutes les fonctionnalités. Finalement c'est toujours la même chose. Comme on a fait l'ajout d'un utilisateur, ajouter une catégorie c'est presque la même chose. C'est même plus simple ;)

On fait dans cet ordre car l'ajout d'article nécessite de choisir une catégorie et de l'associer à l'utilisateur connecté. 
L'ajout d'article est donc le plus "poussé". Notre controller devra récupérer les catégories pour proposer une liste déroulante des catégories dans le formulaire HTML d'ajout (pour chaque catégorie on aura une option avec comme valeur la clef primaire de la catégorie) !
A l'enregistrement (requête SQL) il faudra lier l'id du user connecté avec le jeton pour la colonne user_id !

## Catégories
- Ajouter une catégorie
- Liste des catégories

## Utilisateur
- Ajouter un utilisateur
- Liste des utilisateurs

## Fonctionnalités de sécurisation des pages et session utilisateur.

## Articles
- Ajouter un article
- Listes des articles

## Commentaires
- Liste des commentaires
- Modération(validation ou non) d'un commentaire

## Catégories / Utilisateurs / Articles
- Supprimer
- Modifier

