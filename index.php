<?php
declare(strict_types=1);

/* On inclu la configuration */
require('config/config.php');

/* On inclu la libraire base de données*/
require(PATH_LIB.'bdd.php');
/* On inclu la librairie HTTP */
require(PATH_LIB.'http.php');
/* On inclu la librairie SECUR */
require(PATH_LIB.'secur.php');
/* On inclu la librairie APP */
require(PATH_LIB.'app.php');

/** La constante DEFAULT_CONTROLLER doit-être défini dans le fichier config.php
 * const DEFAULT_CONTROLLER = 'home';
 * C'est le controller par défaut si on en fourni pas. Par exemple *home* qui affichera le Dashbord du backOffice 
 */
$controller = DEFAULT_CONTROLLER;

/** Si le controller est passé dans l'URI on le récupère **/
if(isset($_GET['controller']))
    $controller = $_GET['controller'];

try
{
    /* cont PATH_SRC = 'src/'; définie dans config.php ! */
    $controllerPath = PATH_SRC.'controllers/'.$controller.'.php';

    /* Si le fichier n'existe pas dans le dossier des controller on lève une Exception */
    if(!file_exists($controllerPath))
        throw new DomainException('Le controller "'.$controllerPath.'" n\'existe pas dans le dossier "'.PATH_SRC.'controllers/"');
    
    /* On inclu le controller */
    require($controllerPath);
}
catch (DomainException $e) {
    /** On répond le contenu du fichier error.phtml (template complet) qui affichera le contenu de la variable $message. On précise aussi le code de réponse 404 (ressource non trouvée !)
    * cont PATH_VIEW = 'views/'; //a définir dans config.php
    */
    $errorTitle = '404';
    $message = '<p>'.$e->getMessage().'</p>';
    // On modifie le code de l'entête de la réponse HTTP (404 not found)
    header("HTTP/1.0 404 Not Found");
    require(PATH_VIEWS.'admin/error.phtml');
    exit();
}
catch(PDOException $e)
{
    $errorTitle = '500';
    $message = '<p>Erreur base de donnée :'.$e->getMessage().'</p>';
    header("HTTP/1.0 500 Internal Server Error");
    require(PATH_VIEWS.'admin/error.phtml');
    exit();
}