<?php

// On dit à PHP d'afficher les erreurs pour ce script
ini_set('display_errors', '1');
// On dit à PHP d'afficher les erreur de démarrage pour ce script
ini_set('display_startup_errors', '1');
// On dit à PHP de nous reporter toutes les erreurs 
error_reporting(E_ALL);


/** Paramètres de connexion Mysql */
const DB_DSN = 'mysql:host=localhost;dbname=blog;charset=utf8';
const DB_USER = 'root';
const DB_PASSWORD = '';


/** Les paramètres par défault */
const DEFAULT_CONTROLLER = 'home';

/**  LES ROLES dans l'application */
const ROLES = ['ROLE_ADMIN' => 'Administrateur', 'ROLE_AUTHOR' => 'Auteur'];

/** Les chemins d'accès */
//Répertoire de base de l'application sur le site
define('BASE_DIR', realpath(dirname(__FILE__) . "/../"));
const PATH_SRC = BASE_DIR.'/src/';
const PATH_VIEWS = BASE_DIR.'/views/';
const PATH_LIB = BASE_DIR.'/lib/';
const PATH_UPLOADS = BASE_DIR.'/assets/uploads/';

/** Les URL d'accès pour les vues */
// URL de base de l'application. A paramétrer selon vos dossiers
const BASE_URL = '/blogo/';

const URL_ASSETS = BASE_URL.'/assets/';
const URL_UPLOADS = URL_ASSETS.'/uploads/';