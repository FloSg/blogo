<?php
/* L'utilisateur est-il connecté en admin ?*/
securUserIsConnected('ROLE_ADMIN');

/* Variable générique pour le layout */
const LAYOUT_VIEW = 'admin/user/list';
const LAYOUT_TITLE = 'Liste des utilisateurs' ;

/* Le modèle User */
require (PATH_SRC.'models/user.php');

/* Connexion au SGBD */
$dbh = dbConnect();

/* On récupère tous les utilisateurs */
$users = userFindAll($dbh);

/** Inclu le layout */
require(PATH_VIEWS.'admin/layout.phtml'); 