<?php
/* L'utilisateur est-il connecté en admin ?*/
securUserIsConnected('ROLE_ADMIN');

/* Variable générique pour le layout */
const LAYOUT_VIEW = 'admin/user/add';
const LAYOUT_TITLE = 'Ajouter un utilisateur' ;

/* Le modèle User */
require (PATH_SRC.'models/user.php');


/** Creation d'un tableau d'erreurs vide */
$errors = [];

/* Creation d'un tableau correspondant à mon formulaire avec des valeurs initialisée */
$dataForm = [
    'firstname'  => '',
    'lastname'   => '',
    'email'      => '',
    'password'   => '',
    'bio'        => '',
    'avatar'     => null,
    'valid'      => true,
    'role'       => null
];

if(isset($_POST['firstname'])) {

    /* Le formulaire est posté - On récupère les données de formulaire*/
    $dataForm = [
        'firstname'     => trim($_POST['firstname']),
        'lastname'      => trim($_POST['lastname']),
        'email'         => trim($_POST['email']),
        'password'      => $_POST['password'],
        'createdAt'     => (new DateTime('now'))->format('Y-m-d H:i'),
        'bio'           => $_POST['bio'],
        'avatar'        => null,
        'valid'         => isset($_POST['valid'])?1:0,
        'role'          => isset($_POST['role'])?$_POST['role']:'ROLE_AUTHOR'
    ];

    /* Validation des données transmises */
    if(empty($dataForm['firstname']) || strlen($dataForm['firstname']) < 2)
        $errors['firstname']    = 'Le prénom ne peut-être vide ou inférieur à 2 caractères';

    if(empty($dataForm['lastname']) || strlen($dataForm['lastname']) < 2)
        $errors['lastname']    = 'Le nom ne peut-être vide ou inférieur à 2 caractères';
    
    if(!filter_var($dataForm['email'], FILTER_VALIDATE_EMAIL))
        $errors['email']    = 'L\'email n\'est pas valide';

    if(empty($dataForm['password']) || empty($_POST['passwordConfirmation']))
        $errors['password'] = 'Veuillez renseigner un mot de passe';

    if(strlen($dataForm['password']) < 8)
        $errors['password'] = 'Le mot de passe doit comporter 8 caractères au minimum';

    if($_POST['passwordConfirmation'] != $dataForm['password'])
        $errors['password'] = 'La confirmation du mot de passe n\'est pas bonne';

    /* Si il n'y a pas d'erreur dans les données transmises */
    if(empty($errors)) {

        /** Hash du mot de passe */
        $dataForm['password'] = password_hash($dataForm['password'], PASSWORD_DEFAULT);

        /** On va enregistrer dans la base de données */
        
        $dbh = dbConnect();

        if(userAdd($dbh, $dataForm)) {
            header('Location:'.httpGetUrl('userList'));
            exit();
        }
        else
            $errors['save'] = 'Une erreur a eu lieu lors de l\'enregistrement';
    }
}

/** Inclu le layout */
require(PATH_VIEWS.'admin/layout.phtml'); 