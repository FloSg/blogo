<?php
session_start();
/* L'utilisateur est-il connecté en admin ? */
securUserIsConnected();

/* Variable générique pour le layout */
const LAYOUT_VIEW = 'admin/article/delete';
const LAYOUT_TITLE = 'Supprimer un article' ;

/* Le modèle User */
require (PATH_SRC.'models/article.php');

/* Connexion au SGBD */
$dbh = dbConnect();

/** Le controller ne reçoit pas d'identifiant DANS $_GET ni dans $_POST => on retourne sur la liste */
if(!isset($_REQUEST['id'])) {
    /* Message Flash pour spécifier l'erreur */
    flashbagAdd('warning','Erreur : Aucun identifiant fourni pour la suppression !');

    header('Location:'.httpGetUrl('articleList'));
    exit();
}

/** Le controller reçoit un identifiant dans l'URL
 * On va générer un formulaire de validation pour éviter :
 *  - une mauvaise manipulation
 *  - un retour dans l'historique du navigateur vers un lien de suppression ou pour éviter une faille CSRF
 */
if (isset($_GET['id']) && $_GET['id'] !== '') {
    
    // On récupère l'idenfiant
    $id = (int)$_GET['id'];

    /** On génère un jeton (token) unique que l'on va écrire dans la session
     * Ce jeton sera placé dans un champ caché du formulaire. 
     * A la réception du formulaire, ce jeton sera vérifié avec celui présent dans la session. 
     * S'il ne sont pas identique la suppression sera annulée 
     * Ce procédé permet d'éviter la faille CSRF
     */ 
    $tokenCSRF = uniqid();
    // Ecriture du jeton dans la session
    $_SESSION['tokenCSRF'] = $tokenCSRF;

    // On récupère la catégory dans la base pour afficher son nom dans la demande de confirmation
    $article = articleFindById($dbh, $id);
}

/** Le controller reçoit un identifiant dans les données de formulaire ($_POST)
 * On va vérifier si le jeton CSRF est bon et on supprime la catégorie de la base (et l'image du disque)
 */
if(isset($_POST['id'])) {

    /** Si le jeton transmis par le formulaire n'est pas le même que le jeton de la session
     * on redirige vers la liste avec un message d'alerte !
     */
    if (!isset($_POST['tokenCSRF']) || $_SESSION['tokenCSRF'] != $_POST['tokenCSRF']) {
         /* Message Flash pour spécifier l'erreur */
        flashbagAdd('danger','Erreur : la page de suppression n\'a pas été appelée correctement. Tentative de hack ?');
        header('Location:'.httpGetUrl('articleList'));
        exit();
    }

    // Récupération de l'identifiant
    $id = (int)$_POST['id'];

    // On récupère la catégory dans la base pour savoir s'il y a une image a supprimer
    $article = articleFindById($dbh, $id);

    // Chemin de l'image a supprimer
    $picturePath = PATH_UPLOADS.'category/'.$category['picture'];

    // Si l'image existe on la supprime sur le disque
    if($article['picture']!== null && file_exists($picturePath))
        unlink($picturePath);

    // On supprime la catégorie dans la base
    articleDelete($dbh, $id);

    // Message Flash pour confirmer la suppresion
    flashbagAdd('success','L\'article "'.$article['title'].'" a bien été supprimée !');

    // Retour à la liste
    header('Location:'.httpGetUrl('articleList'));
    exit();
}

/** Inclu le layout */
require(PATH_VIEWS.'admin/layout.phtml'); 