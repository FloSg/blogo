<?php
session_start();

/* Variable générique pour le layout 
Le login est une page standalone
*/
const LAYOUT_VIEW = '';
const LAYOUT_TITLE = '' ;

/* Le modèle User */
require (PATH_SRC.'models/user.php');

$errors = []; //Pas d'erreur pour le moment sur les données

$dataForm = [
    'email'     => '',
];

if(array_key_exists('email',$_POST))
{
    $dataForm['email']      = $_POST['email'];
    $dataForm['password']   = $_POST['password'];

    if(!filter_var($dataForm['email'],FILTER_VALIDATE_EMAIL) || $dataForm['password'] == '')
        $errors[] = 'Merci de vérifier vos identifiants !';

    
    if(count($errors)==0)
    {

        $dbh = dbConnect();
        $sth = $dbh->prepare('SELECT id, lastname, firstname, email, role, valid, password
                            FROM user 
                            WHERE email = :email AND valid=1');
        $sth->bindValue('email', $dataForm['email'] ,PDO::PARAM_STR);
        $sth->execute();
        $user =  $sth->fetch(PDO::FETCH_ASSOC);

        if(password_verify($dataForm['password'],$user['password']))
        {
            //Connexion de l'utilisateur
            $_SESSION['connected'] = true;
            $_SESSION['user'] = ['id'=>$user['id'],'name'=>$user['firstname'].' '.$user['lastname'],'role'=>$user['role']];
            
            //Redirection vers la page userList
            header('Location:'.httpGetUrl('userList'));
            exit();
        
        }
        else
        {
            $errors[] = 'Merci de vérifier vos identifiants !';
        }
    }  
}

/** Inclu la vue login */
require(PATH_VIEWS.'admin/login.phtml'); 