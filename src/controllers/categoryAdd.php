<?php
/* L'utilisateur est-il connecté */
securUserIsConnected('ROLE_AUTHOR');

/* Variable générique pour le layout */
const LAYOUT_VIEW = 'admin/category/add';

define('LAYOUT_TITLE', (isset($_REQUEST['id']))?'Modifier une catégorie':'Ajouter un catégorie');


/* Le modèle User */
require(PATH_SRC.'models/category.php');

/** Creation d'un tableau d'erreur vide */
$errors = [];

/* Creation d'un tableau correspondant à mon formulaire avec des valeurs initialisée */
$dataForm = [
    'id'            => null,
    'title'         => '',
    'description'   => null,
    'picture'       => null,
];

/* On se connecte à la base */
$dbh = dbConnect();


/* Si on reçoit un identifiant : on passe en mode édition. On charge la catégorie dans la base */
if(isset($_GET['id'])) {
    $dataForm = categoryFindById($dbh, $_GET['id']);
}


/** A FAIRE ! créer le programme et la vue ! */
if(isset($_POST['title'])) {

    /* Initialisation du nom de l'image (le nom qui sera enregistré dans la bdd) à null */
    $pictureNameCategory = null;

    /** Traitement de l'upload de l'image */
    if (array_key_exists('picture', $_FILES) && $_FILES['picture']['name'] != '') {
    
        /** On vérifie qu'il n'y ai pas d'erreurs d'upload */
        switch ($_FILES['picture']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                $errors['picture'] = 'Pas de fichier ou erreur sur le fichier';
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $errors['picture'] = 'Fichier trop grand';
                break;
            default:
                $errors['picture'] = 'Erreur inconnu lors du chargement de l\'image !';
        }

        /** Si on a pas d'erreur d'upload on va déplacer l'image dans notre dossier uploads */
        if (count($errors) == 0) {
            
            $info = new SplFileInfo($_FILES['picture']['name']);
            $extension = $info->getExtension();

            /** Je Bascule ça dans le config.php dans une CONSTANTE VALIDE_EXTENSION_PICTURE
             * J'en aurait besoin ailleurs .. avatar utilisateur ;)
             * 
             */
            $valideExtension = ['jpg','jpeg','gif','png','webp'];
            
            if (in_array($extension, $valideExtension)) {
                /** Pour renommer l'image plusieurs possibiltés 
                 * 1. Renommer avec le nom orginal précédé d'un texte unique
                 * 2. Renommer avec le hash du fichier (evite les doublons, mais pas bon pour le référencement - En commentaire)
                */
                $pictureNameCategory = uniqid().'_'.basename($_FILES['picture']['name']);
                //$pictureNameArticle = hash_file('md5', $_FILES['picture']['tmp_name']).'.'.$info->getExtension();

                /** On déplace le fichier temporaire vers sa nouvelle destination */
                move_uploaded_file($_FILES['picture']['tmp_name'], PATH_UPLOADS.'/category/' . $pictureNameCategory);
            }
            else
            {
                $errors['picture'] = 'Ce type de fichier n\'est pas autorisé. Seules les images sont acceptées';
            }
        }
    }

    /* Le formulaire est posté - On récupère les données de formulaire */
    $dataForm = [
        'id'            => isset($_POST['id'])?$_POST['id']:null,
        'title'         => trim($_POST['title']),
        'description'   => trim($_POST['description']) != ''?trim($_POST['description']):null,
        'createdAt'     => (new DateTime('now'))->format('Y-m-d H:i'),
        'picture'       =>  $pictureNameCategory
    ];

    /* Validation des données transmises */
    if(empty($dataForm['title']) || strlen($dataForm['title']) < 2)
        $errors['title']    = 'Le titre ne peut-être vide ou inférieur à 2 caractères';

    /* Si il n'y a pas d'erreur dans les données transmises */
    if(empty($errors)) {
        
        if($dataForm['id'] != null) {
            //Mise à jour
            if(!categoryUpdate($dbh, $dataForm))
                $errors['save'] = 'Une erreur a eu lieu lors de la modification';

            flashbagAdd('success','La catégorie "'.$dataForm['title'].'" a bien été modifiée !');

        }
        else {
            //ajout
            if(!categoryAdd($dbh, $dataForm))
                $errors['save'] = 'Une erreur a eu lieu lors de l\'ajout';
            else
                flashbagAdd('success','La catégorie "'.$dataForm['title'].'" a bien été ajoutée !');

        }
        /** On va enregistrer dans la base de données */
        if(count($errors) == 0) {
            header('Location:'.httpGetUrl('categoryList'));
            exit();
        }
    
    }
}
/** Inclu le layout */
require(PATH_VIEWS.'admin/layout.phtml'); 