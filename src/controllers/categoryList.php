<?php
/* L'utilisateur est-il connecté en admin ?*/
securUserIsConnected();

/* Variable générique pour le layout */
const LAYOUT_VIEW = 'admin/category/list';
const LAYOUT_TITLE = 'Liste des catégories' ;

/* Le modèle User */
require (PATH_SRC.'models/category.php');

/* Connexion au SGBD */
$dbh = dbConnect();

/* On récupère toutes les catégories */
$categories = categoryFindAll($dbh);

/** Génère le token pour le formulaire de suppression dans la MODAL 
 * Voir le controller categoryDel.php pour en savoir plus 
*/
$tokenCSRF = uniqid();
$_SESSION['tokenCSRF'] = $tokenCSRF;


/** Y a t'il des messages Flash à afficher ?
 * L'affichage des messages Flash se fait dans le layout.
 * Une condition vérifie si la variable $flashbag est définie !
*/
$flashbag = flashbagReadAll();

/** Inclu le layout */
require(PATH_VIEWS.'admin/layout.phtml');