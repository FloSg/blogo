<?php

/** Sélectionne tous les articles dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * 
 * @return array le jeu d'enregistrements
 */
function articleFindAll(PDO $dbh)
{
    $stmt = $dbh->prepare('SELECT a.*, c.title AS categoryTitle, u.firstname, u.lastname 
                            FROM article a
                            INNER JOIN category c ON (c.id = a.category_id)
                            INNER JOIN user u ON (u.id = a.user_id)');
    $stmt->execute();
    return $stmt->fetchAll();
}

/** Ajoute un article dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function articleAdd(PDO $dbh, array $data) : bool {
    $stmt = $dbh->prepare('INSERT INTO article (title, content, createdAt, publishedAt, picture, valid, user_id, category_id) 
                    VALUES (:title, :content, :createdAt, :publishedAt, :picture, :valid, :user, :category)');
    /*var_dump($data);
    exit();*/
    unset($data['id']);
    return $stmt->execute($data);
}
/** Met à jour un article dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function updateArticle(PDO $dbh, array $data) : bool {
    unset($data['createdAt']);

    $stmt = $dbh->prepare('UPDATE category
                            SET title = :title, content = :content, picture = :picture, valid = :valid WHERE id = :id');

    return $dbh->execute($data);
}

/** Supprime un article dans la base par son identifiant
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param int $id identifiant de la catégorie
 * 
 * @return array le jeu d'enregistrement
 */
function articleDelete($dbh, $id) {
    $stmt = $dbh->prepare('DELETE FROM article WHERE id = :id');
    $stmt->bindValue('id', $id, PDO::PARAM_INT);
    return $stmt->execute();
}

/** Sélectionne une catégorie dans la base par son identifiant
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param int $id identifiant de la catégorie
 * 
 * @return array le jeu d'enregistrement
 */
function articleFindById(PDO $dbh, int $id) : array
{
    $stmt = $dbh->prepare('SELECT * FROM article WHERE id = :id');
    $stmt->bindValue('id',$id,PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch();
}

/** Met à jour une catégorie dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function articleUpdate(PDO $dbh, array $data) : bool {

    unset($data['createdAt']);
    unset($data['publishedAt']);

    $stmt = $dbh->prepare('UPDATE article 
                            SET title = :title, content = :content, picture = :picture, category_id = :category, valid = :valid, user_id = :user
                            WHERE id = :id');

    return $stmt->execute($data);
}