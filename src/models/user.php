<?php

/** Sélectionne tous les utilisateurs dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * 
 * @return array le jeu d'enregistrements
 */
function userFindAll(PDO $dbh)
{
    $stmt = $dbh->prepare('SELECT * FROM user');
    $stmt->execute();
    return $stmt->fetchAll();
}

/** Sélectionne un utilisateur par son email
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param string $email l'email de l'utilisateur
 * 
 * @return array le jeu d'enregistrement
 */
function userFindByEmail(PDO $dbh, string $email)
{
    $stmt = $dbh->prepare('SELECT * FROM user WHERE email = :email');
    $stmt->execute(['email'=>$email]);
    return $stmt->fetch();
}

/** Ajoute un utilisateur dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function userAdd(PDO $dbh, array $data) : bool {
    $stmt = $dbh->prepare('INSERT INTO user (firstname, lastname, email, password, bio, createdAt, avatar, valid, role) 
                    VALUES (:firstname, :lastname, :email, :password, :bio, :createdAt, :avatar, :valid, :role)');

    return $stmt->execute($data);
}