<?php

/** Sélectionne toutes les catégories dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * 
 * @return array le jeu d'enregistrement
 */
function categoryFindAll(PDO $dbh) : array
{
    $stmt = $dbh->prepare('SELECT * FROM category');
    $stmt->execute();
    return $stmt->fetchAll();
}

/** Sélectionne une catégorie dans la base par son identifiant
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param int $id identifiant de la catégorie
 * 
 * @return array le jeu d'enregistrement
 */
function categoryFindById(PDO $dbh, int $id) : array
{
    $stmt = $dbh->prepare('SELECT * FROM category WHERE id = :id');
    $stmt->bindValue('id',$id,PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch();
}

/** Ajoute une catégorie dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function categoryAdd(PDO $dbh, array $data) : bool {
    unset($data['id']);
    $stmt = $dbh->prepare('INSERT INTO category (title, description, createdAt, picture) 
                    VALUES (:title, :description, :createdAt, :picture)');

    return $stmt->execute($data);
}

/** Met à jour une catégorie dans la base
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param array $data un tableau associatif avec les colonnes à mettre à jour
 * 
 * @return bool un booléen pour notitifier de la bonne éxécution
 */
function categoryUpdate(PDO $dbh, array $data) : bool {

    unset($data['createdAt']);

    $stmt = $dbh->prepare('UPDATE category 
                            SET title = :title, description = :description, picture = :picture 
                            WHERE id = :id');

    return $stmt->execute($data);
}


/** Supprime une catégorie dans la base par son identifiant
 * @param PDO $dbh un objet PDO de connexion à la base
 * @param int $id identifiant de la catégorie
 * 
 * @return array le jeu d'enregistrement
 */
function categoryDelete($dbh, $id) {
    $stmt = $dbh->prepare('DELETE FROM category WHERE id = :id');
    $stmt->bindValue('id',$id,PDO::PARAM_INT);
    return $stmt->execute();
}